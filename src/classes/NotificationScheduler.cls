global class NotificationScheduler implements Schedulable {
    /* * * * * * * * * * * * *
    *  Method Name:  execute
    *  Purpose:      This method notify the public group members for vehicles submitted today
    * * * * * * * * * * * * */
   global void execute( SchedulableContext schedulableContext ) {
      VehicleSubmittedNotificationBatch objVehicleSubmittedBatch = new VehicleSubmittedNotificationBatch();
      Database.executeBatch( objVehicleSubmittedBatch );
   }
}
