@isTest(SeeAllData=false)
private class NotificationSchedulerTest {
    /* * * * * * * * * * * * *
    *  Method Name:  test_VehicleSubmittedNotification
    *  Purpose:      This method notify the public group members for vehicles submitted today
    * * * * * * * * * * * * */
    public static testMethod void testExecute() {
        Test.startTest();
            NotificationScheduler objNotificationScheduler = new NotificationScheduler();
            String schCron = '0 0 0 * * ? *';
            String jobId = System.Schedule('Test Email send',schCron,objNotificationScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
                SELECT Id, CronExpression, TimesTriggered,NextFireTime
                FROM CronTrigger
                WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression);
        System.assertEquals( 0, cronTrigger.TimesTriggered );
    }
}
