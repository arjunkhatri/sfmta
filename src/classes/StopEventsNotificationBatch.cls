/**
* @author Arjun Khatri
* @date 01/09/17
*
* @group Notifications
*
* @description Batch apex on Stop Event that notify at the first day of the month 
* to the shuttle company to input new values for new stop events.
*/
public with sharing class StopEventsNotificationBatch implements Database.Batchable<sObject>{

    /*******************************************************************************************************
    * @description This method fetch all the stop events which was created last month
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @return the collection of stop events which was created last month
    */        
    public Database.QueryLocator start( Database.BatchableContext bc ) {
        String query = 'SELECT Id ';
               query+= 'FROM {0} WHERE ';
               query+= 'CreatedDate = LAST_MONTH ';
               query = String.format(query, new LIST<String>{ ''+Stop_Schedule__c.SObjectType });
        return Database.getQueryLocator(query);
    }

    /*******************************************************************************************************
    * @description This method clone the stop event which was created last month
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @param scope is a collection of stop event which was created last month
    */        
    public void execute( Database.BatchableContext bc, List<Stop_Schedule__c > scope ) {
        Boolean emailIsSendOrNot;

        if( !scope.isEmpty() ) {
            emailIsSendOrNot = StopEventsNotificationService.cloneAllStopEventsAndStopJunctions( scope );
        }

    }

    public void finish( Database.BatchableContext bc ) {
    }
}