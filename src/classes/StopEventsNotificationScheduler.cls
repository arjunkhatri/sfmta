/**
* @author Arjun Khatri
* @date 07/09/17
*
* @group Notifications
*
* @description Schedulable apex to schedule batch first of every month
* to notify to the shuttle company to input new values
*/
global class StopEventsNotificationScheduler implements Schedulable {
    /*******************************************************************************************************
    * @description This Method schedule batch first of every monthto notify to Account's related contact
    * @param schedulableContext is a context variable which store the runtime information
    */
    global void execute( SchedulableContext schedulableContext ) {
        StopEventsNotificationBatch objStopEventsNotificationBatch = new StopEventsNotificationBatch();
        Database.executeBatch( objStopEventsNotificationBatch , Integer.valueOf( System.label.Stop_Event_Notification_Batch_Size ) );
    }
}