@isTest
private class StopEventsNotificationSchedulerTest {
    
    /*******************************************************************************************************
    * @description This Method schedule batch first of every month to notify to Account's related contact
    */
    public static testMethod void testExecute() {
        Test.startTest();
            StopEventsNotificationScheduler objStopEventScheduler = new StopEventsNotificationScheduler();
            String schCron = '0 0 0 1 * ? *';
            String jobId = System.Schedule('Test Email send',schCron,objStopEventScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
            SELECT Id
                 , CronExpression
                 , TimesTriggered
                 , NextFireTime
            FROM CronTrigger
           WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression  , 'Cron expression should match' );
        System.assertEquals( 0, cronTrigger.TimesTriggered  , 'Time to Triggered batch should be 0' );
        List<AsyncapexJob> apexJobs = [
            SELECT Id, ApexClassID ,JobType , Status 
            FROM AsyncapexJob
            WHERE JobType = 'BatchApex'];
        System.assertEquals( false , apexJobs.isEmpty()  , 'Expected at least one Async apex Job' );
        System.assertEquals( 'BatchApex', apexJobs[0].JobType  , 'Job Type should match ' );
    }
}