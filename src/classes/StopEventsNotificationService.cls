/**
* @author Arjun Khatri
* @date 01/09/17
*
* @group Notifications
*
* @description Service class that clone all the last month stop events and stop junctions
* and also send email to all account's related contacts.
*/
public with sharing class StopEventsNotificationService {

    public static final String CONTACT_RECORD_TYPE      = 'External_Contact';
    public static final String EMAIL_SUBJECT_LINE       = 'Stop Event Generated For {0}';

    /*******************************************************************************************************
    * @description This method get all the last month stop events and stop junctions to clone
    * @param scope is a collection of stop event which was created last month
    * @return the collection of last month stop events and stop junctions to clone
    */
    public static List<Stop_Schedule__c > getAllStopEventsToClone( List<Stop_Schedule__c > scope ) {

        List<String> stopScheduleFieldsToCopy = new List<String>();
        List<String> stopJunctionFieldsToCopy = new List<String>();

        List<Stop_Schedule_Field__mdt> stopScheduleSetting = [
            SELECT DeveloperName, Field_To_Copy__c
            FROM Stop_Schedule_Field__mdt
        ];

        List<Stop_Junction_Field__mdt> stopjunctionSetting = [
            SELECT DeveloperName, Field_To_Copy__c
            FROM Stop_Junction_Field__mdt
        ];

        if( !stopScheduleSetting.isEmpty() ) {
            stopScheduleFieldsToCopy.addAll( Pluck.strings( Stop_Schedule_Field__mdt.Field_To_Copy__c , stopScheduleSetting ) );
        }

        if( !stopjunctionSetting.isEmpty() ) {
            stopJunctionFieldsToCopy.addAll( Pluck.strings( Stop_Junction_Field__mdt.Field_To_Copy__c , stopjunctionSetting ) );
        }

        String query = 'SELECT {0} , ( SELECT {1} ';
            query+= ' FROM Stop_Junctions__r WHERE CreatedDate = LAST_MONTH )';
            query+= ' FROM {2} WHERE Id IN: scope ';
            query = String.format(query, new LIST<String>{ String.join( stopScheduleFieldsToCopy , ', ' ) 
                                                     , String.join( stopJunctionFieldsToCopy , ', ' )
                                                     , ''+Stop_Schedule__c.SObjectType });

        return database.query(query);
    }

    /*******************************************************************************************************
    * @description This method clone all the last month stop events and stop junctions
    * @param allLastMonthStopEvents is a collection of stop event and Stop Junctions which was created last month
    */
    public static Boolean cloneAllStopEventsAndStopJunctions( List<Stop_Schedule__c > allLastMonthStopEvents ) {

        Map<Id,Stop_Schedule__c> stopSchedulesToClone = new Map<Id,Stop_Schedule__c>();
        List<Stop_Junction__c> stopJunctionToClone = new List<Stop_Junction__c>();

        if( allLastMonthStopEvents.isEmpty() ){
            return null;
        }

        Decimal daysOfOperation = [
            Select DeveloperName, Days_Of_Operation__c
            FROM Days_Of_Operation__mdt LIMIT 1 
        ].Days_Of_Operation__c;

        for( Stop_Schedule__c objStopEvent : getAllStopEventsToClone( allLastMonthStopEvents ) ) {
            if( !objStopEvent.Stop_Junctions__r.isEmpty() ) {
                stopJunctionToClone.addAll( objStopEvent.Stop_Junctions__r );
            }

             Stop_Schedule__c cloneStopEvent = objStopEvent.clone(false);
             cloneStopEvent.STL_Days_Of_Operation__c = daysOfOperation;
             stopSchedulesToClone.put( objStopEvent.Id , cloneStopEvent );
        }

        if( !stopSchedulesToClone.values().isEmpty() ) {
            insert stopSchedulesToClone.values();
        }

        return cloneAllStopJunctions( stopJunctionToClone , stopSchedulesToClone );
    }    

    /*******************************************************************************************************
    * @description This method clone all the last month stop events and stop junctions
    * @param stopJunctionToClone is a collection of stop Junctions which was created last month
    * @param newlyCreatedStopSchedules is a map of newly created stop event id to stop event
    */
    public static Boolean cloneAllStopJunctions( List<Stop_Junction__c> stopJunctionToClone 
                                            , Map<Id,Stop_Schedule__c> newlyCreatedStopSchedules ) {
        Boolean emailIsSendOrNot;
        List<Stop_Junction__c> stopJunctionsToClone = new List<Stop_Junction__c>();

        if( stopJunctionToClone.isEmpty() ){
            return null;
        }

        for( Stop_Junction__c objStopJunction : stopJunctionToClone ) {
            Stop_Junction__c cloneStopJunction = objStopJunction.clone(false);
            cloneStopJunction.Stop_Schedule__c = newlyCreatedStopSchedules.get( objStopJunction.Stop_Schedule__c ).Id;
            stopJunctionsToClone.add( cloneStopJunction );
        }

        if( !stopJunctionsToClone.isEmpty() ) {
            insert stopJunctionsToClone;
        }

        if( !newlyCreatedStopSchedules.isEmpty() ) {
            emailIsSendOrNot = sendEmailToContact( newlyCreatedStopSchedules );
        }

        return emailIsSendOrNot;
        
    }

    /*******************************************************************************************************
    * @description This method notify at the first day of the month to the shuttle company
    * to input new values for new stop events.
    * @param newlyCreatedStopSchedules is a map of newly created stop event id to stop event
    */ 
    public static Boolean sendEmailToContact( Map<Id,Stop_Schedule__c> newlyCreatedStopSchedules ) {
        List<Messaging.SingleEmailMessage> notificationEmails = new List<Messaging.SingleEmailMessage>();
        VehicleNotificationService.EmailResult objEmailResult = new VehicleNotificationService.EmailResult();

        if( newlyCreatedStopSchedules.values().isEmpty() ) {
            return null;
        }

        String template  =  '{0}, <br/><br/> Please update your monthly stop events and operating days by {1} 1st. ';
               template += '<a href="https://sf-mta.force.com/commutershuttles/s/recordlist/Stop_Schedule__c/Recent">Link To Record</a> <br/><br/>';
               template += 'Even if you have no changes to your monthly stop events please confirm that the number of operating days is correct for your company. ';
               template += 'You will be charged based on the number of operating days and no refunds will be given once monthly invoices are issued.<br/><br/><br/>';
               template += 'Thanks,<br/><br/>SFMTA Commuter Shuttle Team';

        for( Account objAccount : getRecordsToSendEmail( newlyCreatedStopSchedules ) ) {
            String bodyOfEmail = '';
            String subjectOfEmail = '';
            Set<Id> contactsIdsToSendEmail = new Set<Id>();
            if( !objAccount.Contacts.isEmpty() ) {
                for ( Contact objContact : objAccount.Contacts ) {
                   if( !String.isBlank( objContact.Email ) ) {
                       contactsIdsToSendEmail.add( objContact.Id  ) ;
                   }
                }
            }

            if( !objAccount.Stop_Schedules__r.isEmpty() ) {
                bodyOfEmail = String.format( template, new LIST<String>{ ''+objAccount.Name 
                                                                       , ''+objAccount.Stop_Schedules__r[0].Name });
                subjectOfEmail = String.format( EMAIL_SUBJECT_LINE, new LIST<String>{ ''+objAccount.Stop_Schedules__r[0].Name });
            }

            if( !contactsIdsToSendEmail.isEmpty() ) {
                notificationEmails.addAll( VehicleNotificationService.generateSingleEmailMessages( subjectOfEmail 
                                                                                                 , bodyOfEmail
                                                                                                 , contactsIdsToSendEmail ) ) ;
            }
        }
        if( !notificationEmails.isEmpty() ) {
            objEmailResult = VehicleNotificationService.sendEmail( notificationEmails );
        }

        return objEmailResult.isSuccess;
    }

    /*******************************************************************************************************
    * @description This method fetch all the Accoun't related contact and stop event whom to send email
    * @param newlyCreatedStopSchedules is a map of newly created stop event id to stop event
    * @return collection of Accoun't related contact and stop event whom to send email
    */ 
    public static List<Account> getRecordsToSendEmail( Map<Id,Stop_Schedule__c> newlyCreatedStopSchedules ) {
        Map<Id, Set<sObject> > operatorToStopEvents = new Map<Id, Set<sObject> >();
        List<Account> accountRelatedContactsToSendEmail = new List<Account>();

        operatorToStopEvents.putAll( Pluck.fieldToSObjects( Stop_Schedule__c.STL_Operator__c , newlyCreatedStopSchedules.values() ) );

        if( !operatorToStopEvents.isEmpty() && operatorToStopEvents.keySet() != null ) {
            accountRelatedContactsToSendEmail = [
                SELECT Id , Name , 
                ( SELECT Id , Email
                  From Contacts 
                  Where RecordType.DeveloperName =: CONTACT_RECORD_TYPE
                ),
                ( SELECT Id , Name
                  From Stop_Schedules__r 
                  Where CreatedDate = LAST_MONTH
                )
                FROM Account
                Where Id IN: operatorToStopEvents.keySet() 
            ];
        }

        return accountRelatedContactsToSendEmail;
    }
}