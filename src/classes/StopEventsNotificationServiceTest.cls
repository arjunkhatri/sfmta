@isTest
private class StopEventsNotificationServiceTest {
    
    /*******************************************************************************************************
    * @description This method get all the last month stop events and stop junctions to clone
    */
    public static testMethod void test_GetAllStopEventsToClone() {
        User adminUser = SObjectFactory.ADMIN_USER;
        System.runAs( adminUser ) {
            Id RecordTypeIdContact = [
                SELECT Id
                FROM RecordType
                WHERE SObjectType='Contact' 
                AND DeveloperName='External_Contact'
                AND IsActive = TRUE LIMIT 1
            ].Id;               
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
               .put(Contact.RecordTypeId , RecordTypeIdContact)
               .put(Contact.AccountId, testAccount)
               .put(Contact.Email, 'testxxx000@exmaple.com')
               .count(testAccount.size()).create().getRecords();                
            List<Stop_Schedule__c> stopSchedulesToClone = new SObjectBuilder(Stop_Schedule__c.sObjectType)
                .put(Stop_Schedule__c.STL_Operator__c, testAccount)
                .count(testAccount.size()).create().getrecords();
            Datetime lastMonth = Datetime.now().addMonths(-1);
            for( Stop_Schedule__c objStopSchedule : stopSchedulesToClone ) {
                Test.setCreatedDate( objStopSchedule.Id, lastMonth );
            }
            List<Stops__c> stops = new SObjectBuilder(Stops__c.sObjectType)
                .count(testAccount.size()).create().getrecords();
            List<Stop_Junction__c> stopJunctionsToClone = new SObjectBuilder(Stop_Junction__c.sObjectType)
                .put(Stop_Junction__c.Stop_Schedule__c, stopSchedulesToClone)
                .put(Stop_Junction__c.Stop__c, stops)
                .count(testAccount.size()).create().getrecords();
            for( Stop_Junction__c objStopJunction : stopJunctionsToClone ) {
                Test.setCreatedDate( objStopJunction.Id, lastMonth );
            }
            Test.startTest();
                List<Stop_Schedule__c > stopEventWithJunctionToClone = StopEventsNotificationService.getAllStopEventsToClone( stopSchedulesToClone );
            Test.stopTest();
            System.assertEquals( 5 , stopEventWithJunctionToClone.size() , 'Expected stop events count equals to five' );
        }
    }

    /*******************************************************************************************************
    * @description This method clone all the last month stop events and stop junctions
    */
    public static testMethod void test_CloneAllStopEventsAndStopJunctions() {
        User adminUser = SObjectFactory.ADMIN_USER;
        System.runAs( adminUser ) {
            Id RecordTypeIdContact = [
                SELECT Id
                FROM RecordType
                WHERE SObjectType='Contact' 
                AND DeveloperName='External_Contact'
                AND IsActive = TRUE LIMIT 1
            ].Id;            
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
               .put(Contact.RecordTypeId , RecordTypeIdContact)
               .put(Contact.AccountId, testAccount)
               .put(Contact.Email, 'testxxx000@exmaple.com')
               .count(testAccount.size()).create().getRecords();               
            List<Stop_Schedule__c> stopSchedulesToClone = new SObjectBuilder(Stop_Schedule__c.sObjectType)
                .put(Stop_Schedule__c.STL_Operator__c, testAccount)
                .count(testAccount.size()).create().getrecords();
            Datetime lastMonth = Datetime.now().addMonths(-1);
            for( Stop_Schedule__c objStopSchedule : stopSchedulesToClone ) {
                Test.setCreatedDate( objStopSchedule.Id, lastMonth );
            }
            List<Stops__c> stops = new SObjectBuilder(Stops__c.sObjectType)
                .count(testAccount.size()).create().getrecords();
            List<Stop_Junction__c> stopJunctionsToClone = new SObjectBuilder(Stop_Junction__c.sObjectType)
                .put(Stop_Junction__c.Stop_Schedule__c, stopSchedulesToClone)
                .put(Stop_Junction__c.Stop__c, stops)
                .count(testAccount.size()).create().getrecords();
            for( Stop_Junction__c objStopJunction : stopJunctionsToClone ) {
                Test.setCreatedDate( objStopJunction.Id, lastMonth );
            }
            Boolean emailStatusMessage;                   
            Test.startTest();
                emailStatusMessage = StopEventsNotificationService.cloneAllStopEventsAndStopJunctions( stopSchedulesToClone );
            Test.stopTest();
            System.assertEquals( stopSchedulesToClone.size() ,  [Select Id FROM Stop_Schedule__c WHERE Id != :stopSchedulesToClone].size(), 'The stop schedules should be cloned' );            
            System.assertEquals( stopJunctionsToClone.size() ,  [Select Id FROM Stop_Junction__c WHERE Id != :stopJunctionsToClone].size(), 'The stop junctions should be cloned' );
            System.assertEquals( true , emailStatusMessage , 'Expected email should be sent successfully' );
        }
    }
}