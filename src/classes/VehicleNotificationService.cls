/**
* @author Arjun Khatri
* @date 24/08/17
*
* @group Notifications
*
* @description Service class that get user's from public group , roles , subordinate roles
* and also send email to public group , roles , subordinate roles users.
*/
public with sharing class VehicleNotificationService {

    public static final String GROUP_TYPE_REGULAR   = 'Regular';
    public static final String EMAIL_STATUS_SUCCESS = 'Success';

    /*******************************************************************************************************
    * @description This method notify the public group members for vehicles submitted today
    * @param bodyOfEmail the content of the email to send to group members
    * @param recIds the collection of record ids to whom notify about vehicles
    * @return the collection of single email messages
    */
    public static List<Messaging.SingleEmailMessage> generateSingleEmailMessages( String subjectOfEmail 
                                                                                , String bodyOfEmail 
                                                                                , Set<Id> recIds ) {
        List<Id> allRecipientIds = new List<Id>();
        String emailStatusMessage = '';

        if( !recIds.isEmpty() ) {
            allRecipientIds.addAll( recIds );
        }
        List<Messaging.SingleEmailMessage> notificationEmails = new List<Messaging.SingleEmailMessage>();
        for ( List<String> objSendToAddress : getMailAddressInChunk(allRecipientIds) ) {
            Messaging.SingleEmailMessage objEmailTemplate = new Messaging.SingleEmailMessage();
            objEmailTemplate.setToAddresses( objSendToAddress );
            objEmailTemplate.setSubject( subjectOfEmail );
            objEmailTemplate.setHtmlBody( bodyOfEmail );
            notificationEmails.add( objEmailTemplate );
        }
        return notificationEmails;
    }

    /*******************************************************************************************************
    * @description This method collects user's email address in chunk of 100 from group and its subgroup
    * @param allRecipientIds The collection of all user's email addresses for ublic group
    * @return the collection of user's email address in chunk for public group
    */
    public static List<List<String>> getMailAddressInChunk( List<Id> allRecipientIds ) {
        List<List<String>> splittedRecipientMailAddress = new List<List<String>>();
        Integer chunkCount = 100;
        Integer remainingCount = 0;
        for( Integer index = 0; index < allRecipientIds.size(); index = index + chunkCount ) {
            List<String> chunkMailAddresses = new List<String>();
            remainingCount = chunkCount < ( allRecipientIds.size() - index ) ? chunkCount :  allRecipientIds.size() - index;
            for( Integer nextIndex = index ; nextIndex < index + remainingCount ; nextIndex++ ) {
                chunkMailAddresses.add( allRecipientIds[nextIndex] );
            }
            splittedRecipientMailAddress.add( chunkMailAddresses );
        }
        return splittedRecipientMailAddress;
    }

    /*******************************************************************************************************
    * @description This method collects all user's id from group and its subgroup
    * @param strGroupName the name of the public group to get members emails
    * @return the collection of user's id for the given public group
    */
    public static Set<id> getUserIdsFromGroup( String strGroupName ) {
        Set<Id> usersIdToGetEmail = new Set<Id>();
        Set<Id> subGroupId = new Set<Id>();
        String userTypePrefix = Schema.SObjectType.User.getKeyPrefix();

        for( Group objGroup : [ 
            SELECT Id,Type ,DeveloperName,RelatedId
            , ( SELECT Id, UserOrGroupId FROM GroupMembers LIMIT 100)
            FROM Group
            WHERE Name =: strGroupName
            AND RelatedId = null ]) {
            if( !objGroup.GroupMembers.isEmpty() ) {
                for ( GroupMember objGroupMem : objGroup.GroupMembers ) {
                    if( ( (Id)objGroupMem.UserOrGroupId).getSObjectType() == User.SObjectType ) {
                        usersIdToGetEmail.add( objGroupMem.UserOrGroupId );
                    }
                    else if ( objGroup.Type == GROUP_TYPE_REGULAR ) {
                        subGroupId.add( objGroupMem.UserOrGroupId );
                    }
                }
            }
        }
        if( !subGroupId.isEmpty() ) {
            usersIdToGetEmail.addAll(getAllUserIdFromRoleIds(subGroupId));
        }
        return usersIdToGetEmail;
    }

    /*******************************************************************************************************
    * @description  This method collects all user's id from group of roles
    * @param setGroupIds the collection of id's of the public group to get members emails
    * @return the collection of user's id for given collection of id's of groups
    */
    public static Set<Id> getAllUserIdFromRoleIds( Set<Id> setGroupIds ) {
        Set<Id> relatedRoleIdToGetUsersId = new Set<Id>();
        Set<Id> userRoleIdToGetUsersId = new Set<Id>();
        Set<Id> usersIdToGetEmail = new Set<Id>();
        Map<Id,Group> groupMap = new Map<Id, Group>([
            SELECT Id,Type ,DeveloperName,RelatedId
            FROM Group
            WHERE Id IN: setGroupIds
            AND RelatedId != null
        ]);
        if( !groupMap.isEmpty() ) {
            relatedRoleIdToGetUsersId.addAll( Pluck.ids( Group.RelatedId , groupMap ) );
        }

        if( !relatedRoleIdToGetUsersId.isEmpty() ) {
            userRoleIdToGetUsersId.addAll( getAllSubordinateRoleIds( relatedRoleIdToGetUsersId ) );
        }
        Map<Id,User> idToUserMap = new Map<Id, User>([
            SELECT Id, Name
            FROM User
            WHERE UserRoleId IN :userRoleIdToGetUsersId
        ]);
        if( !idToUserMap.isEmpty() ) {
            usersIdToGetEmail.addAll( Pluck.ids( idToUserMap ) );
        }
        return usersIdToGetEmail;
    }


    /*******************************************************************************************************
    * @description This is recursive method which collects all subordinate user's id
    * @param roleIds the collection of the users role id's
    * @return the collection of all subordinate role ids for given collection of the users role id's
    */
    public static Set<Id> getAllSubordinateRoleIds( Set<Id> roleIds ) {
        Integer level = 1;
        Integer SUPPORTING_LEVEL = 4;
        Set<Id> currentRoleIds = new Set<Id>();
        List<UserRole> usersRole = [
            SELECT Id
            FROM UserRole
            WHERE ParentRoleId IN :roleIds
            AND ParentRoleId != null
        ];
        if( !usersRole.isEmpty() ) {
            currentRoleIds.addAll( Pluck.ids( usersRole ) );
        }
        if( !currentRoleIds.isEmpty() && level <= SUPPORTING_LEVEL ) {
            currentRoleIds.addAll(getAllSubordinateRoleIds(currentRoleIds));
            level++;
        } else{
            currentRoleIds.addAll(roleIds);
        }
        return currentRoleIds;
    }

    /*******************************************************************************************************
    * @description This method notify the Account's related contact for vehicles rejected today
    * @param accountsToGetContact the collection of all the rejected vehicles accounts and its related contact
    * @return the message that email send or not
    */
    public static EmailResult sendEmailToContact( String subjectOfEmail , String bodyOfEmail 
                                           , List<Account> accountsToGetContact ) {

        List<Messaging.SingleEmailMessage> notificationEmails = new List<Messaging.SingleEmailMessage>();

        if( accountsToGetContact.isEmpty() ) {
            return null;
        }
        Set<Id> contactsIdsToSendEmail = new Set<Id>();
        for( Account objAccount : accountsToGetContact ) {

            if( !objAccount.Contacts.isEmpty() ) {
                for ( Contact objContact : objAccount.Contacts ) {
                   if( !String.isBlank( objContact.Email ) ) {
                       contactsIdsToSendEmail.add( objContact.Id  ) ;
                   }
                }
            }

            if( !contactsIdsToSendEmail.isEmpty() ) {
                notificationEmails.addAll( generateSingleEmailMessages( subjectOfEmail 
                                                                      , String.format( bodyOfEmail, new LIST<String>{ ''+objAccount.Name }) 
                                                                      , contactsIdsToSendEmail ) ) ;
            }
        }
        EmailResult objEmailResult = sendEmail( notificationEmails );

        if( !objEmailResult.failedIds.isEmpty() ) {
            contactsIdsToSendEmail.removeAll( objEmailResult.failedIds );
        }

        objEmailResult.succeededIds.addAll( contactsIdsToSendEmail );

        return objEmailResult;
    }


    /*******************************************************************************************************
    * @description This method notify the public group for Account's related vehicles submitted today
    * @param accountsToGetContact the collection of all the submitted vehicles accounts
    * @return the message that email send or not
    */
    public static EmailResult sendEmailToPublicGroup( String subjectOfEmail , String bodyOfEmail 
                                                    , List<Account> accountsToSendEmail , String publicGroupName ) {

        List<Messaging.SingleEmailMessage> notificationEmails = new List<Messaging.SingleEmailMessage>();

        if( accountsToSendEmail.isEmpty() ) {
            return null;
        }

        Set<Id> usersIdsToSendEmail = getUserIdsFromGroup( publicGroupName );

        for( Account objAccount : accountsToSendEmail ) {

            if( !usersIdsToSendEmail.isEmpty() ) {
                notificationEmails.addAll( generateSingleEmailMessages( subjectOfEmail 
                                                                      , String.format( bodyOfEmail, new LIST<String>{ ''+objAccount.Name }) 
                                                                      , usersIdsToSendEmail ) );
            }
        }

        EmailResult objEmailResult = sendEmail( notificationEmails );

        if( !objEmailResult.failedIds.isEmpty() ) {
            usersIdsToSendEmail.removeAll( objEmailResult.failedIds );
        }
        
        objEmailResult.succeededIds.addAll( usersIdsToSendEmail );

        return objEmailResult;        
    }

    /*******************************************************************************************************
    * @description This method notify the respective recipient
    * @param notificationEmails the collection of single email messages to send
    * @return the message that email send or not
    */
    public static EmailResult sendEmail( List<Messaging.SingleEmailMessage> notificationEmails ) {

        String emailStatusMessage = '';

        if( notificationEmails.isEmpty() ) {
            return null;
        }

        EmailResult emailResultObj = new EmailResult();
        emailResultObj.isSuccess = true;

        Messaging.SendEmailResult[] sendEmailStatus = Messaging.sendEmail( notificationEmails );

        for( Messaging.SendEmailResult objSendEmailResult : sendEmailStatus ) {
            if( !objSendEmailResult.isSuccess() ) {
                for( Messaging.SendEmailError objSendEmailError : objSendEmailResult.getErrors() ) {
                    emailResultObj.failedIds.add( objSendEmailError.getTargetObjectId() );
                    emailResultObj.errorMessages.add( objSendEmailError.getMessage() );
                }
                emailResultObj.isSuccess = false;
            }
        }

        return emailResultObj;
    }

    /*******************************************************************************************************
    * @description Wrapper class to hold the information for email result
    */
    public class EmailResult {
        public Set<Id> succeededIds;
        public Set<Id> failedIds;
        public List<String> errorMessages;
        public Boolean isSuccess;
        public EmailResult() {
            succeededIds = new Set<Id>();
            failedIds = new Set<Id>();
            errorMessages = new List<String>();
        }
    }

    /*******************************************************************************************************
    * @description This method update all the vehicles that email has been sent
    * @param accountsToGetVehicles the collection of all the expiring vehicles accounts and its related contact
    */
    public static void updateVehicles( List<Account> accountsToGetVehicles ) {

        List<Vehicle__c> vehiclesToUpdate = new List<Vehicle__c>();
        
        if( !accountsToGetVehicles.isEmpty() ) {
            for( Vehicle__c objVehicle : getVehiclesToUpdate( accountsToGetVehicles ) ) {
                objVehicle.Sent__c = true;
                vehiclesToUpdate.add( objVehicle );
            }
        }

        if( !vehiclesToUpdate.isEmpty() ) {
            update vehiclesToUpdate;
        }
    }

    /*******************************************************************************************************
    * @description This method get all the vehicles to update
    * @param accountsToGetVehicles the collection of all the expiring vehicles accounts and its related contact
    * @return the collection of vehicles to update
    */
    public static List<Vehicle__c> getVehiclesToUpdate( List<Account> accountsToGetVehicles ) {
        List<Vehicle__c> allVehicleToUpdate = new List<Vehicle__c>();

        for( Account objAccount : accountsToGetVehicles ) {
            if( !objAccount.Vehicles__r.isEmpty() ) {
                allVehicleToUpdate.addAll( objAccount.Vehicles__r );
            }
        }

        return allVehicleToUpdate;
    }
}