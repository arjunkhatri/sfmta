@isTest
private class VehicleNotificationServiceTest {

    /*******************************************************************************************************
    * @description This method collects all user's id from group and its subgroup
    */
    public static testMethod void test_GetUserIdsFromGroupAndItsNestedGroup() {
        User adminUser = SObjectFactory.ADMIN_USER;
        Group testGroup = (Group)new SObjectBuilder(Group.sObjectType)
           .create().getRecord();
        Group testNestedGroup = (Group)new SObjectBuilder(Group.sObjectType)
          .create().getRecord();
        List<GroupMember> groupMembersAssociateWithGroup = new SObjectBuilder(GroupMember.sObjectType)
               .put(GroupMember.GroupId, testGroup)
               .put(GroupMember.UserOrGroupId, testNestedGroup)
               .count(1).create().getrecords();
        List<GroupMember> groupMembersForNestedGroup = new SObjectBuilder(GroupMember.sObjectType)
               .put(GroupMember.GroupId, testNestedGroup)
               .put(GroupMember.UserOrGroupId, adminUser)
               .count(1).create().getrecords();
        Test.startTest();
            Set<Id> usersIdToGetEmail = VehicleNotificationService.getUserIdsFromGroup( testGroup.Name );
        Test.stopTest();
        System.assertEquals( 1 , usersIdToGetEmail.size() , 'The list should have one user id' );
    }

    /*******************************************************************************************************
    * @description This method collects all user's role id from group of roles
    */    
    public static testMethod void test_GetUserRoleIdsFromGroupRoles() {
        List<UserRole> userRoles = new SObjectBuilder(UserRole.sObjectType)
               .put(UserRole.Name , 'Test Role')
               .count(1).create().getrecords();
        User adminUser = (User)new SObjectBuilder(User.sObjectType)
           .put(User.UserRoleId , userRoles)
           .createAsAdmin().getRecord();
        Group testGroup = (Group)new SObjectBuilder(Group.sObjectType)
           .create().getRecord();
        List<Group> roleGroup = [Select Id From Group Where Type = 'Role' AND RelatedId =: userRoles[0].Id];
        List<GroupMember> groupMembersAssociateWithGroup = new SObjectBuilder(GroupMember.sObjectType)
               .put(GroupMember.GroupId, testGroup)
               .put(GroupMember.UserOrGroupId, roleGroup)
               .count(1).create().getrecords();
        Test.startTest();
            Set<Id> usersIdsToSendEmail = VehicleNotificationService.getUserIdsFromGroup( testGroup.Name );
        Test.stopTest();
        System.assertEquals( 1 , usersIdsToSendEmail.size() , 'The list should have one user id' );
    }

    /*******************************************************************************************************
    * @description This method notify the public group members for vehicles submitted today
    */   
    public static testMethod void test_GenerateSingleEmailMessages() {
        User adminUser = SObjectFactory.ADMIN_USER;
       Group testGroup = (Group)new SObjectBuilder(Group.sObjectType)
          .create().getRecord();
       List<GroupMember> groupMembersAssociateWithGroup = new SObjectBuilder(GroupMember.sObjectType)
              .put(GroupMember.GroupId, testGroup)
              .put(GroupMember.UserOrGroupId, adminUser)
              .count(1).create().getrecords();
        System.runAs( adminUser ) {
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();

            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, testAccount)
                .put(Vehicle__c.License_Plate__c, testAccount)
                .count(testAccount.size()).create().getrecords();
            List<Messaging.SingleEmailMessage> notificationEmails = new List<Messaging.SingleEmailMessage>();
            String emailStatusMessage = '';
            Test.startTest();
                String vehiclesName = '';
                for ( Vehicle__c objVehicle : vehiclesToSendEmail ) {
                    vehiclesName += objVehicle.Name + ', ';
                }
                vehiclesName += vehiclesName.removeEnd(', ');
                String bodyOfEmail = 'Hi Team ,<br/> Below are all vehicle that got submitted today : <br/>' + vehiclesName;
                notificationEmails.addAll( VehicleNotificationService.generateSingleEmailMessages( 'Vehicles approved or rejected'  
                                         , bodyOfEmail 
                                         , VehicleNotificationService.getUserIdsFromGroup( testGroup.Name ) ));            
            Test.stopTest();
            System.assertEquals( 1 , notificationEmails.size() , 'Expected single email messages count equals to one' );
        }

    }

    /*******************************************************************************************************
    * @description This method notify the Account's related contact for vehicles rejected today
    */
    public static testMethod void test_SendEmailToContact() {
        User adminUser = SObjectFactory.ADMIN_USER;
        System.runAs( adminUser ) {
            Id RecordTypeIdContact = [
                SELECT Id
                FROM RecordType
                WHERE SObjectType='Contact' 
                AND DeveloperName='External_Contact'
                AND IsActive = TRUE LIMIT 1
            ].Id;            
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
               .put(Contact.RecordTypeId , RecordTypeIdContact)
               .put(Contact.AccountId, testAccount)
               .put(Contact.Email, 'testxxx000@exmaple.com')
               .count(testAccount.size()).create().getRecords();
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, testAccount)
                .put(Vehicle__c.License_Plate__c, testAccount)
                .put(Vehicle__c.Status__c ,'Rejected')
                .put(Vehicle__c.STL_Rejection_Reason__c ,'Expired or Incorrect Registration')
                .count(testAccount.size()).create().getrecords();
            VehicleNotificationService.EmailResult objEmailResult = new VehicleNotificationService.EmailResult();
            List<Account> accountsToGetContact = [
                SELECT id , Name 
                , (SELECT Id,Account__c,Name FROM Vehicles__r WHERE Status__c = 'Rejected' AND LastModifiedDate = TODAY )
                , ( SELECT Id , AccountId ,Email FROM Contacts  WHERE RecordType.DeveloperName = 'External_Contact')
                FROM Account WHERE id IN : testAccount
            ];
            Test.startTest();
                objEmailResult = VehicleNotificationService.sendEmailToContact( 'Vehicles approved or rejected'
                                                                              , 'Test Body' 
                                                                              , accountsToGetContact );
            Test.stopTest();
            System.assertEquals( true , objEmailResult.isSuccess , 'Expected email should be sent successfully' );
        }
    }

    /*******************************************************************************************************
    * @description This method collects user's email address in chunk of 100 from group and its subgroup
    */
    public static testMethod void test_GetChunkUserMailAddresses() {
        List<User> adminUsers = new SObjectBuilder(User.sObjectType)
           .count(500).createAsAdmin().getRecords();
        Group testGroup = (Group)new SObjectBuilder(Group.sObjectType)
           .create().getRecord();
        List<GroupMember> groupMembersAssociateWithGroup = new SObjectBuilder(GroupMember.sObjectType)
               .put(GroupMember.GroupId, testGroup)
               .put(GroupMember.UserOrGroupId, adminUsers)
               .count(1).create().getrecords();
        Test.startTest();
            List<Id> allRecipientIds = new List<Id>();
            Set<Id> usersIdsToSendEmail = VehicleNotificationService.getUserIdsFromGroup( testGroup.Name );
            if( !usersIdsToSendEmail.isEmpty() ) {
                allRecipientIds.addAll( usersIdsToSendEmail );
            }        
            List<List<String>> splittedRecipientMailAddress = VehicleNotificationService.getMailAddressInChunk( allRecipientIds );
        Test.stopTest();
        System.assertEquals( 1 , usersIdsToSendEmail.size() , 'Expected user Ids count equals to one' );
        System.assertEquals( 1 , splittedRecipientMailAddress.size() , 'Expected set of user email address equals to one' );
    }

    /*******************************************************************************************************
    * @description This method notify the public group for Account's related vehicles submitted today
    */
    public static testMethod void test_SendEmailToPublicGroup() {
        User adminUser = SObjectFactory.ADMIN_USER;
        Group testGroup = (Group)new SObjectBuilder(Group.sObjectType)
           .create().getRecord();
        List<GroupMember> groupMembersAssociateWithGroup = new SObjectBuilder(GroupMember.sObjectType)
               .put(GroupMember.GroupId, testGroup)
               .put(GroupMember.UserOrGroupId, adminUser)
               .count(1).create().getrecords();      
        System.runAs( adminUser ) {
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, testAccount)
                .put(Vehicle__c.License_Plate__c, testAccount)
                .put(Vehicle__c.Status__c ,'Submitted - Pending Approval')
                .count(testAccount.size()).create().getrecords();
            VehicleNotificationService.EmailResult objEmailResult = new VehicleNotificationService.EmailResult();
            List<Account> accountsToSendEmail = [
                SELECT id , Name 
                , (SELECT Id,Account__c,Name FROM Vehicles__r WHERE Status__c = 'Rejected' AND LastModifiedDate = TODAY )
                FROM Account WHERE id IN : testAccount
            ];
            Test.startTest();
                objEmailResult = VehicleNotificationService.sendEmailToPublicGroup( 'Vehicles approved or rejected' 
                                            , 'Test Body' , accountsToSendEmail , testGroup.Name );
            Test.stopTest();
            System.assertEquals( true , objEmailResult.isSuccess , 'Expected email should be sent successfully' );
        }
    }

    /*******************************************************************************************************
    * @description This method notify the respective recipient
    */
    public static testMethod void test_SendEmail() {
        User adminUser = SObjectFactory.ADMIN_USER;
        System.runAs( adminUser ) {
            Id RecordTypeIdContact = [
                SELECT Id
                FROM RecordType
                WHERE SObjectType='Contact' 
                AND DeveloperName='External_Contact'
                AND IsActive = TRUE LIMIT 1
            ].Id;             
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
               .put(Contact.RecordTypeId , RecordTypeIdContact)
               .put(Contact.AccountId, testAccount)
               .put(Contact.Email, 'testxxx000@exmaple.com')
               .count(testAccount.size()).create().getRecords();
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, testAccount)
                .put(Vehicle__c.License_Plate__c, testAccount)
                .put(Vehicle__c.Status__c ,'Rejected')
                .put(Vehicle__c.STL_Rejection_Reason__c ,'Expired or Incorrect Registration')
                .count(testAccount.size()).create().getrecords();

            List<Messaging.SingleEmailMessage> notificationEmails = new List<Messaging.SingleEmailMessage>();
            VehicleNotificationService.EmailResult objEmailResult = new VehicleNotificationService.EmailResult();
            Test.startTest();
                String bodyOfEmail = 'Test Body';
                notificationEmails.addAll( VehicleNotificationService.generateSingleEmailMessages( 'Vehicles approved or rejected' 
                                            , bodyOfEmail 
                                            , new Set<Id>{testContact[0].Id} ));
                objEmailResult = VehicleNotificationService.sendEmail( notificationEmails );       
            Test.stopTest();
            System.assertEquals( true , objEmailResult.isSuccess , 'Expected email should be sent successfully' );
        }
    }

    /*******************************************************************************************************
    * @description This method get all the vehicles to update
    */    
    public static testMethod void test_GetVehiclesToUpdate() {

        Id RecordTypeIdContact = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Contact' 
            AND DeveloperName='External_Contact'
            AND IsActive = TRUE LIMIT 1
        ].Id;            
        List<Account> testAccounts = new SObjectBuilder(Account.sObjectType)
           .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
           .count(5).create().getRecords();
        List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
           .put(Contact.AccountId, testAccounts )
           .put(Contact.Email, 'testxxx000@exmaple.com')
           .put(Contact.RecordTypeId , RecordTypeIdContact)
           .count(testAccounts.size()).create().getRecords();
        List<Vehicle__c> testVehicles = new SObjectBuilder(Vehicle__c.sObjectType)
            .put(Vehicle__c.Account__c, testAccounts)
            .put(Vehicle__c.License_Plate__c, testAccounts)
            .put(Vehicle__c.Registration_Expiration_Date__c ,  Date.Today().addDays(12))
            .count(testAccounts.size()).create().getrecords();
        List<Account> accountsToGetVehicles = [
            SELECT Id , Name 
            , (SELECT Id FROM Vehicles__r WHERE Registration_Expiration_Date__c = NEXT_N_DAYS:14  AND Sent__c = false )
            FROM Account WHERE Id IN : testAccounts
        ];
        Test.startTest();
            List<Vehicle__c> allVehicleToUpdate = VehicleNotificationService.getVehiclesToUpdate( accountsToGetVehicles );
        Test.stopTest();
        System.assertEquals( testAccounts.size() , allVehicleToUpdate.size() , 'The number of vehicles should match the test vehicles' );

    }
    /*******************************************************************************************************
    * @description This method update all the vehicles that email has been sent
    */    
    public static testMethod void test_UpdateVehicles() {

        Id RecordTypeIdContact = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Contact' 
            AND DeveloperName='External_Contact'
            AND IsActive = TRUE LIMIT 1
        ].Id;            
        List<Account> testAccounts = new SObjectBuilder(Account.sObjectType)
           .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
           .count(5).create().getRecords();
        List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
           .put(Contact.AccountId, testAccounts )
           .put(Contact.Email, 'testxxx000@exmaple.com')
           .put(Contact.RecordTypeId , RecordTypeIdContact)
           .count(testAccounts.size()).create().getRecords();
        List<Vehicle__c> testVehicles = new SObjectBuilder(Vehicle__c.sObjectType)
            .put(Vehicle__c.Account__c, testAccounts)
            .put(Vehicle__c.License_Plate__c, testAccounts)
            .put(Vehicle__c.Registration_Expiration_Date__c ,  Date.Today().addDays(12))
            .count(testAccounts.size()).create().getrecords();
        List<Account> accountsToGetVehicles = [
            SELECT Id , Name 
            , (SELECT Id FROM Vehicles__r WHERE Registration_Expiration_Date__c = NEXT_N_DAYS:14  AND Sent__c = false )
            FROM Account WHERE Id IN : testAccounts
        ];
        Test.startTest();
            VehicleNotificationService.updateVehicles( accountsToGetVehicles );
        Test.stopTest();
        List<Vehicle__c> updatedVehicle = [ 
            SELECT Sent__c 
            FROM Vehicle__c 
            WHERE Id IN: testVehicles 
            AND Sent__c = true 
        ];
        System.assertEquals( testVehicles.size() ,  updatedVehicle.size() , 'The number of vehicles should match the test vehicles' );
        for( Vehicle__c objVehicle : updatedVehicle ) {
            System.assertEquals( true ,  objVehicle.Sent__c , 'The Vehicle should be updated' );
        }
    }
}