/**
* @author Arjun Khatri
* @date 14/09/17
*
* @group Notifications
*
* @description Batch apex on Account that notify at the first day of the month 
* to the shuttle staff for all vehicles with expired registration from the previous month.
*/
public class VehiclePreviousMonthExpirationBatch implements Database.Batchable<sObject> {

    public static final String PUBLIC_GROUP_NAME    = System.label.Vehicle_Submitted_Notification_Group;
    public static final String EMAIL_SUBJECT_LINE   = 'Vehicles Expired In Last Month';
    public VehicleNotificationService.EmailResult objEmailResult = new VehicleNotificationService.EmailResult();

    /***********************************************************************************
    * @description This method fetch all the Account which have vehicles with expired 
    * registration from the previous month
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @return the collection of Account with vehicles with expired registration
    * from the previous month
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id ';
               query+= 'FROM {0} WHERE Id IN ( SELECT Account__c ';
               query+= 'FROM {1} WHERE Registration_Expiration_Date__c = LAST_MONTH )';
               query = String.format(query, new LIST<String>{ ''+Account.SObjectType
                                                            , ''+Vehicle__c.SObjectType });
        return Database.getQueryLocator(query);
    }

    /******************************************************************************************
    * @description This method notify at the first day of the month to the shuttle staff
    * for all vehicles with expired registration from the previous month
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @param scope is a collection of Account whose vehicles registration expired in last month
    */
    public void execute(Database.BatchableContext BC, List<Account> scope) {

        List<Account> accountsToSendEmail =  [
            SELECT Id , Name , 
            ( SELECT Id,Account__c,Name 
              FROM Vehicles__r 
              WHERE Registration_Expiration_Date__c = LAST_MONTH 
            )
            FROM Account 
            WHERE Id IN: scope
        ];

        String bodyOfEmail =  '{0} , <br/><br/> One or more vehicles registration expired in last month which was submitted by your company. ';
               bodyOfEmail += 'Please log in to the portal to view the vehicles. ';

        if( !accountsToSendEmail.isEmpty() ) {
            objEmailResult = VehicleNotificationService.sendEmailToPublicGroup( EMAIL_SUBJECT_LINE  , bodyOfEmail 
                                                                                , accountsToSendEmail , PUBLIC_GROUP_NAME );
        }
    }
    
    public void finish(Database.BatchableContext BC) {
    }
    
}