@isTest
private class VehiclePreviousMonthExpirationBatchTest {

    /*******************************************************************************************************
    * @description This method notify at the first day of the month to the shuttle staff 
    * for all vehicles with expired registration from the previous month
    */
    public static testMethod void test_VehiclePreviousMonthExpirationBatch() {
        User adminUser = SObjectFactory.ADMIN_USER;
        VehiclePreviousMonthExpirationBatch batchCls = new VehiclePreviousMonthExpirationBatch();
        System.runAs( adminUser ) {
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            Datetime lastMonth = Datetime.now().addMonths(-1);
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, testAccount)
                .put(Vehicle__c.License_Plate__c, testAccount)
                .put(Vehicle__c.Registration_Expiration_Date__c,Date.valueOf( lastMonth ) )
                .count(testAccount.size()).create().getrecords();          
            Test.startTest();
                Database.executeBatch(batchCls);
            Test.stopTest();
        }
    }

    /*******************************************************************************************************
    * @description This method notify at the first day of the month to the shuttle staff 
    * for all vehicles with expired registration from the previous month
    */
    public static testMethod void test_Execute() {
        User adminUser = SObjectFactory.ADMIN_USER;
        VehiclePreviousMonthExpirationBatch batchCls = new VehiclePreviousMonthExpirationBatch();
        System.runAs( adminUser ) {
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            Datetime lastMonth = Datetime.now().addMonths(-1);
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, testAccount)
                .put(Vehicle__c.License_Plate__c, testAccount)
                .put(Vehicle__c.Registration_Expiration_Date__c,Date.valueOf( lastMonth ) )
                .count(testAccount.size()).create().getrecords();
            Test.startTest();
                batchCls.execute(null,testAccount);
                batchCls.finish(null);
            Test.stopTest();
            System.assertEquals( true , batchCls.objEmailResult.isSuccess , 'Expected email should be sent successfully' );
        }
    }

}