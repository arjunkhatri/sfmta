/**
* @author Arjun Khatri
* @date 28/08/17
*
* @group Notifications
*
* @description Batch apex on Account that notify related contact at the end of the day
* when multiple vehicles got rejected YESTERDAY.
*/
public with sharing class VehicleRejectedNotificationBatch implements Database.Batchable<sObject> {

    public static final String VEHICLE_STATUS             = 'Rejected';
    public static final String CONTACT_RECORD_TYPE        = 'External_Contact';
    public static final String EMAIL_SUBJECT_LINE         = 'Vehicles approved or rejected';
    public VehicleNotificationService.EmailResult objEmailResult = new VehicleNotificationService.EmailResult();


    /*******************************************************************************************************
    * @description This method fetch all the accounts which have vehicles rejected YESTERDAY
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @return the collection of accounts whose vehicles got rejected YESTERDAY 
    */
    public Database.QueryLocator start( Database.BatchableContext bc ) {
        String query = 'SELECT Id ';
               query+= 'FROM {0} WHERE Id IN ( SELECT Account__c ';
               query+= 'FROM {1} WHERE Status__c =: VEHICLE_STATUS AND LastModifiedDate = YESTERDAY )';
               query = String.format(query, new LIST<String>{ ''+Account.SObjectType
                                                            , ''+Vehicle__c.SObjectType });
        return Database.getQueryLocator(query);
    }

    /*******************************************************************************************************
    * @description This method notify the Account's related contact for vehicles rejected YESTERDAY
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @param scope is a collection of accounts whose vehicles got rejected YESTERDAY
    */        
    public void execute( Database.BatchableContext bc, List<Account> scope ) {

        List<Account> accountsToGetContact =  [
            SELECT Id , Name ,
            ( SELECT Id , AccountId ,Email 
              FROM Contacts 
              WHERE RecordType.DeveloperName =: CONTACT_RECORD_TYPE) 
            FROM Account 
            WHERE Id IN: scope
        ];

        String bodyOfEmail =  '{0}, <br/><br/> The SFMTA has approved or rejected one or more vehicles submitted by your company. Please log in to the portal to view the vehicles. <br/><br/>';
               bodyOfEmail += 'For those vehicles which have been rejected, please look at the "rejection reason" field in the vehicle record on the portal. ';
               bodyOfEmail += 'After you resolve the issue listed there, change the vehicle status to "submitted" and we will review. <br/><br/>';
               bodyOfEmail += 'Permit authorization stickers for approved vehicles will be mailed to the address listed on your account unless you request to pick them up at our offices.';

        if( !accountsToGetContact.isEmpty() ) {
            objEmailResult = VehicleNotificationService.sendEmailToContact( EMAIL_SUBJECT_LINE , bodyOfEmail , accountsToGetContact );
        }
    }

    public void finish( Database.BatchableContext bc ) {
    }
}