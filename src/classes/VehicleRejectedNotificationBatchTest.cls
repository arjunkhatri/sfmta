@isTest
private class VehicleRejectedNotificationBatchTest {
  
    /*******************************************************************************************************
    * @description This method notify the Account's related contact for vehicles rejected yesterday
    */
    public static testMethod void test_VehicleRejectedNotification() {
 
        User adminUser = SObjectFactory.ADMIN_USER;
        VehicleRejectedNotificationBatch batchCls = new VehicleRejectedNotificationBatch();
        System.runAs( adminUser ) {
            Id RecordTypeIdContact = [
                SELECT Id
                FROM RecordType
                WHERE SObjectType='Contact' 
                AND DeveloperName='External_Contact'
                AND IsActive = TRUE LIMIT 1
            ].Id;
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(1).create().getRecords();
            List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
               .put(Contact.AccountId, new SObjectParentProviders.GenericParentProvider())
               .put(Contact.Email, 'testxxx000@exmaple.com')
               .put(Contact.RecordTypeId , RecordTypeIdContact)
               .count(1).create().getRecords();
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, new SObjectParentProviders.GenericParentProvider())
                .put(Vehicle__c.Status__c ,'Rejected')
                .put(Vehicle__c.STL_Rejection_Reason__c ,'Expired or Incorrect Registration')
                .put(Vehicle__c.License_Plate__c, 'VH5245')
                .put(Vehicle__c.LastModifiedDate, Datetime.now().addDays(-1) )
                .count(testAccount.size()).metabuild().getrecords();
            Test.startTest();
                Database.executeBatch(batchCls);
            Test.stopTest();
        }
    }

    /*******************************************************************************************************
    * @description This method updated the vehicles notify the Account's related contact for vehicles rejected yesterday
    */        
    public static testMethod void test_Execute() {
        User adminUser = SObjectFactory.ADMIN_USER;
        List<Vehicle__c> assertVehicleList = new List<Vehicle__c>();
        VehicleRejectedNotificationBatch batchCls = new VehicleRejectedNotificationBatch();
        System.runAs( adminUser ) {
            Id RecordTypeIdContact = [
                SELECT Id
                FROM RecordType
                WHERE SObjectType='Contact' 
                AND DeveloperName='External_Contact'
                AND IsActive = TRUE LIMIT 1
            ].Id;
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(1).create().getRecords();
            List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
               .put(Contact.RecordTypeId , RecordTypeIdContact)
               .put(Contact.AccountId, testAccount)
               .put(Contact.Email, 'testxxx000@exmaple.com')
               .count(1).create().getRecords();
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, new SObjectParentProviders.GenericParentProvider())
                .put(Vehicle__c.Status__c ,'Rejected')
                .put(Vehicle__c.STL_Rejection_Reason__c ,'Expired or Incorrect Registration')
                .put(Vehicle__c.License_Plate__c, 'VH5245')
                .put(Vehicle__c.LastModifiedDate, Datetime.now().addDays(-1) )
                .count(testAccount.size()).metabuild().getrecords();
            Test.startTest();
                batchCls.execute(null,testAccount);
                batchCls.finish(null);
            Test.stopTest();
            System.assertEquals( true , batchCls.objEmailResult.isSuccess , 'Expected email should be sent successfully' );
        }
    }
}