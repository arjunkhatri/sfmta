/**
* @author Arjun Khatri
* @date 31/08/17
*
* @group Notifications
*
* @description Schedulable apex to schedule batch daily at the end of the day
* to notify to Account's related contact for vehicles rejected today
*/
global class VehicleRejectedNotificationScheduler implements Schedulable {

    /*******************************************************************************************************
    * @description This Method schedule batch daily at the end of the day to notify to Account's related contact
    * @param schedulableContext is a context variable which store the runtime information
    */
    global void execute( SchedulableContext schedulableContext ) {
        VehicleRejectedNotificationBatch objVehicleRejectedBatch = new VehicleRejectedNotificationBatch();
        Database.executeBatch( objVehicleRejectedBatch , Integer.valueOf( System.label.Vehicle_Rejected_Notification_Batch_Size ) );
    }
}