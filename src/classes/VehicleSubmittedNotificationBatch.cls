/**
* @author Arjun Khatri
* @date 23/08/17
*
* @group Notifications
*
* @description Batch apex on Vehicle that notify to public group members at the end of the day
* when new vehicles got submit for approval on YESTERDAY.
*/
public with sharing class VehicleSubmittedNotificationBatch implements Database.Batchable<sObject>{

    public static final String PUBLIC_GROUP_NAME    = System.label.Vehicle_Submitted_Notification_Group;
    public static final String VEHICLE_STATUS       = 'Submitted - Pending Approval';
    public static final String EMAIL_SUBJECT_LINE   = 'Vehicles approved or rejected';

    /*******************************************************************************************************
    * @description This method fetch all the accounts which have vehicles submitted YESTERDAY
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @return the collection of accounts whose vehicles got submitted YESTERDAY 
    */        
    public Database.QueryLocator start( Database.BatchableContext bc ) {
        String query = 'SELECT Id ';
               query+= 'FROM {0} WHERE Id IN ( SELECT Account__c ';
               query+= 'FROM {1} WHERE Status__c =: VEHICLE_STATUS AND CreatedDate = YESTERDAY )';
               query = String.format(query, new LIST<String>{ ''+Account.SObjectType
                                                            , ''+Vehicle__c.SObjectType });
        return Database.getQueryLocator(query);
    }

    /*******************************************************************************************************
    * @description This method notify the Account's for vehicles submitted YESTERDAY
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @param scope is a collection of accounts whose vehicles got submitted YESTERDAY
    */     
    public void execute( Database.BatchableContext bc, List<Account> scope ) {
        VehicleNotificationService.EmailResult objEmailResult = new VehicleNotificationService.EmailResult();

        List<Account> accountsToSendEmail =  [
            SELECT Id , Name , 
            ( SELECT Id,Account__c,Name 
              FROM Vehicles__r 
              WHERE Status__c =: VEHICLE_STATUS
              AND CreatedDate = YESTERDAY )
            FROM Account 
            WHERE Id IN: scope
        ];

        String bodyOfEmail =  '{0}, <br/><br/> The SFMTA has approved or rejected one or more vehicles submitted by your company. Please log in to the portal to view the vehicles. <br/><br/>';
               bodyOfEmail += 'For those vehicles which have been rejected, please look at the "rejection reason" field in the vehicle record on the portal. ';
               bodyOfEmail += 'After you resolve the issue listed there, change the vehicle status to "submitted" and we will review. <br/><br/>';
               bodyOfEmail += 'Permit authorization stickers for approved vehicles will be mailed to the address listed on your account unless you request to pick them up at our offices.';

        if( !accountsToSendEmail.isEmpty() ) {
            objEmailResult = VehicleNotificationService.sendEmailToPublicGroup( EMAIL_SUBJECT_LINE  , bodyOfEmail 
                                                                                , accountsToSendEmail , PUBLIC_GROUP_NAME );
        }
    }

    public void finish( Database.BatchableContext bc ) {
    }
}