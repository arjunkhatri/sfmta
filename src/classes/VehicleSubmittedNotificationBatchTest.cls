@isTest
private class VehicleSubmittedNotificationBatchTest {

    /*******************************************************************************************************
    * @description This method notify the public group members for vehicles submitted yesterday
    */    
    public static testMethod void test_VehicleSubmittedNotification() {
        User adminUser = SObjectFactory.ADMIN_USER;
        VehicleSubmittedNotificationBatch batchCls = new VehicleSubmittedNotificationBatch();
        System.runAs( adminUser ) {
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c,  new SObjectParentProviders.GenericParentProvider())
                .put(Vehicle__c.Status__c ,'Submitted - Pending Approval')
                .put(Vehicle__c.License_Plate__c, testAccount)
                .count(testAccount.size()).create().getrecords();
            Datetime yesterday = Datetime.now().addDays(-1);   
            for( Vehicle__c objVehicle : vehiclesToSendEmail ) {
                Test.setCreatedDate( objVehicle.Id, yesterday );
            }
            Test.startTest();
                Database.executeBatch(batchCls);
            Test.stopTest();
        }
    }

    /*******************************************************************************************************
    * @description This method update the vehicle and notify the public group members for vehcile submitted yesterday
    */    
    public static testMethod void test_UpdateVehicletoSubmitted() {
        User adminUser = SObjectFactory.ADMIN_USER;
        List<Vehicle__c> assertVehicleList = new List<Vehicle__c>();
        VehicleSubmittedNotificationBatch batchCls= new VehicleSubmittedNotificationBatch();
        System.runAs( adminUser ) {
            List<Account> testAccount = new SObjectBuilder(Account.sObjectType)
               .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
               .count(5).create().getRecords();
            List<Vehicle__c> vehiclesToSendEmail = new SObjectBuilder(Vehicle__c.sObjectType)
                .put(Vehicle__c.Account__c, new SObjectParentProviders.GenericParentProvider())
                .put(Vehicle__c.License_Plate__c, testAccount)
                .count(testAccount.size()).create().getrecords();
            Datetime yesterday = Datetime.now().addDays(-1);    
            for( Vehicle__c objVehicle : vehiclesToSendEmail ) {
                Test.setCreatedDate( objVehicle.Id, yesterday );
            }                
            Test.startTest();
                for(Vehicle__c vehicleObj : [SELECT Id,
                                                    Name,
                                                    Status__c
                                               FROM Vehicle__c
                                            ]) {
                    vehicleObj.Status__c = 'Submitted - Pending Approval';
                    assertVehicleList.add(vehicleObj);
                }
                update assertVehicleList;
                Database.executeBatch(batchCls);
            Test.stopTest();
        }
    }
}