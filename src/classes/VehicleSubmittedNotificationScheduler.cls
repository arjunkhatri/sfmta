/**
* @author Arjun Khatri
* @date 28/08/17
*
* @group Notifications
*
* @description Schedulable apex to schedule batch daily at the end of the day
* to notify to public group members
*/
global class VehicleSubmittedNotificationScheduler implements Schedulable {

    /*******************************************************************************************************
    * @description This Method schedule batch daily at the end of the day to notify to public group members
    * @param schedulableContext is a context variable which store the runtime information
    */
   global void execute( SchedulableContext schedulableContext ) {
      VehicleSubmittedNotificationBatch objVehicleSubmittedBatch = new VehicleSubmittedNotificationBatch();
      Database.executeBatch( objVehicleSubmittedBatch , Integer.valueOf( System.label.Vehicle_Submitted_Notification_Batch_Size ) );
   }
}