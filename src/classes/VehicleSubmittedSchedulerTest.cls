@isTest
private class VehicleSubmittedSchedulerTest {

    /*******************************************************************************************************
    * @description This method notify the public group members for vehicles submitted today
    */
    public static testMethod void testExecute() {
        Test.startTest();
            VehicleSubmittedNotificationScheduler objNotificationScheduler = new VehicleSubmittedNotificationScheduler();
            String schCron = '0 0 0 * * ? *';
            String jobId = System.Schedule('Test Email send',schCron,objNotificationScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
            SELECT Id
                 , CronExpression
                 , TimesTriggered
                 , NextFireTime
            FROM CronTrigger
           WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression  , 'Cron expression should match' );
        System.assertEquals( 0, cronTrigger.TimesTriggered  , 'Time to Triggered batch should be 0' );
        List<AsyncapexJob> apexJobs = [
            SELECT Id, ApexClassID ,JobType , Status 
            FROM AsyncapexJob
            WHERE JobType = 'BatchApex'];
        System.assertEquals( false , apexJobs.isEmpty()  , 'Expected at least one Async apex Job' );
        System.assertEquals( 'BatchApex', apexJobs[0].JobType  , 'Job Type should match ' );
    }
}