/**
* @author Arjun Khatri
* @date 11/09/17
*
* @group Notifications
*
* @description Batch apex on Account that notify at the end of the day 
* to the shuttle company that they have vehicle registrations expiring in next 14 days.
*/
public class VehicleUpcomingExpirationBatch implements Database.Batchable<sObject> {

    public static final String EMAIL_SUBJECT_LINE                = 'Vehicles Expire In next 14 Days';
    public static final String CONTACT_RECORD_TYPE               = 'External_Contact';
    public VehicleNotificationService.EmailResult objEmailResult = new VehicleNotificationService.EmailResult();

    /*******************************************************************************************************
    * @description This method fetch all the Account which have vehicles registraion expire in next 14 days
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @return the collection of Account whose vehicles will expire in next 14 days
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id ';
               query+= 'FROM {0} WHERE Id IN ( SELECT Account__c ';
               query+= 'FROM {1} WHERE Registration_Expiration_Date__c = NEXT_N_DAYS:14 ';
               query+= 'AND Sent__c = false )';
               query = String.format(query, new LIST<String>{ ''+Account.SObjectType
                                                            , ''+Vehicle__c.SObjectType });
        return Database.getQueryLocator(query);
    }

    /*****************************************************************************************************************
    * @description This method notify at the end of the day to the shuttle company for vehicles expire in next 14 days
    * @param bc is a context variable which store the runtime information (jobid etc)
    * @param scope is a collection of Account whose vehicles will expire in next 14 days
    */
    public void execute(Database.BatchableContext BC, List<Account> scope) {

        List<Account> accountsToGetContact =  [
            SELECT Id , Name ,
            ( SELECT Id , Sent__c  
              FROM Vehicles__r 
              WHERE Registration_Expiration_Date__c = NEXT_N_DAYS:14 
              AND Sent__c = false),            
            ( SELECT Id , AccountId ,Email 
              FROM Contacts 
              WHERE RecordType.DeveloperName =: CONTACT_RECORD_TYPE) 
            FROM Account 
            WHERE Id IN: scope
        ];

        String bodyOfEmail =  '{0} , <br/><br/> One or more vehicles will expire in next 14 days which was submitted by your company. ';
               bodyOfEmail += 'Please log in to the portal to view the vehicles. ';

        if( !accountsToGetContact.isEmpty() ) {
            objEmailResult = VehicleNotificationService.sendEmailToContact( EMAIL_SUBJECT_LINE , bodyOfEmail , accountsToGetContact );
        }

        if( objEmailResult != null && !objEmailResult.succeededIds.isEmpty() ) {
            List<Account> accountsToUpdateVehicles =  [
                SELECT Id , Name ,
                ( SELECT Id , Sent__c  
                  FROM Vehicles__r 
                  WHERE Registration_Expiration_Date__c = NEXT_N_DAYS:14 
                  AND Sent__c = false)
                FROM Account 
                WHERE Id IN ( SELECT AccountId 
                              FROM Contact
                              WHERE RecordType.DeveloperName =: CONTACT_RECORD_TYPE
                              AND Id IN: objEmailResult.succeededIds
                            )
                AND Id NOT IN ( SELECT AccountId 
                              FROM Contact
                              WHERE RecordType.DeveloperName =: CONTACT_RECORD_TYPE
                              AND Id IN: objEmailResult.failedIds
                            )
            ];
            VehicleNotificationService.updateVehicles( accountsToUpdateVehicles );
        }
    }

    public void finish(Database.BatchableContext BC) {
        
    }

}