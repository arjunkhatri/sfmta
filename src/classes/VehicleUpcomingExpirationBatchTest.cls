@isTest
private class VehicleUpcomingExpirationBatchTest {
    /******************************************************************************************************************
    * @description This method notify at the end of the day to the shuttle company for vehicles expire in next 14 days
    */
    public static testMethod void test_VehicleUpcomingExpirationBatch() {

        VehicleUpcomingExpirationBatch batchCls = new VehicleUpcomingExpirationBatch();

        Id RecordTypeIdContact = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Contact' 
            AND DeveloperName='External_Contact'
            AND IsActive = TRUE LIMIT 1
        ].Id;            
        List<Account> testAccounts = new SObjectBuilder(Account.sObjectType)
           .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
           .count(5).create().getRecords();
        List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
           .put(Contact.AccountId, testAccounts )
           .put(Contact.Email, 'testxxx000@exmaple.com')
           .put(Contact.RecordTypeId , RecordTypeIdContact)
           .count(testAccounts.size()).create().getRecords();
        List<Vehicle__c> testVehicles = new SObjectBuilder(Vehicle__c.sObjectType)
            .put(Vehicle__c.Account__c, testAccounts)
            .put(Vehicle__c.License_Plate__c, testAccounts)
            .put(Vehicle__c.Registration_Expiration_Date__c ,  Date.Today().addDays(12))
            .count(testAccounts.size()).create().getrecords();
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
        List<Vehicle__c> updatedVehicle = [ 
            SELECT Sent__c 
            FROM Vehicle__c 
            WHERE Id IN: testVehicles 
            AND Sent__c = true 
        ];
        System.assertEquals( testVehicles.size() ,  updatedVehicle.size() , 'The number of vehicles should match the test vehicles' );
        for( Vehicle__c objVehicle : updatedVehicle ) {
            System.assertEquals( true ,  objVehicle.Sent__c , 'The Vehicle should be updated' );
        }
    }
    /******************************************************************************************************************
    * @description This method notify at the end of the day to the shuttle company for vehicles expire in next 14 days
    */
    public static testMethod void test_Execute() {

        VehicleUpcomingExpirationBatch batchCls = new VehicleUpcomingExpirationBatch();

        Id RecordTypeIdContact = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Contact' 
            AND DeveloperName='External_Contact'
            AND IsActive = TRUE LIMIT 1
        ].Id;            
        List<Account> testAccounts = new SObjectBuilder(Account.sObjectType)
           .put(Account.Name, new SObjectFieldProviders.UniqueStringProvider())
           .count(5).create().getRecords();
        List<Contact> testContact = new SObjectBuilder(Contact.sObjectType)
           .put(Contact.AccountId, testAccounts )
           .put(Contact.Email, 'testxxx000@exmaple.com')
           .put(Contact.RecordTypeId , RecordTypeIdContact)
           .count(testAccounts.size()).create().getRecords();
        List<Vehicle__c> testVehicles = new SObjectBuilder(Vehicle__c.sObjectType)
            .put(Vehicle__c.Account__c, testAccounts)
            .put(Vehicle__c.License_Plate__c, testAccounts)
            .put(Vehicle__c.Registration_Expiration_Date__c ,  Date.Today().addDays(12))
            .count(testAccounts.size()).create().getrecords();
        List<Account> accountsToGetVehicles = [
            SELECT Id , Name 
            , (SELECT Id , Sent__c FROM Vehicles__r WHERE Registration_Expiration_Date__c = NEXT_N_DAYS:14  AND Sent__c = false )
            FROM Account WHERE Id IN : testAccounts
        ];          
        Test.startTest();
            batchCls.execute( null , accountsToGetVehicles );
        Test.stopTest();
        List<Vehicle__c> updatedVehicle = [ 
            SELECT Sent__c 
            FROM Vehicle__c 
            WHERE Id IN: testVehicles 
            AND Sent__c = true 
        ];
        System.assertEquals( testVehicles.size() ,  updatedVehicle.size() , 'The number of vehicles should match the test vehicles' );
        for( Vehicle__c objVehicle : updatedVehicle ) {
            System.assertEquals( true ,  objVehicle.Sent__c , 'The Vehicle should be updated' );            
        }
        System.assertEquals( true , batchCls.objEmailResult.isSuccess , 'Expected email should be sent successfully' );
    }
}