/**
* @author Arjun Khatri
* @date 13/09/17
*
* @group Notifications
*
* @description Schedulable apex to schedule batch daily at the end of the day
* to the shuttle company that they have vehicle registrations expiring in next 14 days.
*/
global class VehicleUpcomingExpirationScheduler implements Schedulable {

    /*******************************************************************************************************
    * @description This Method schedule batch daily at the end of the day to notify to Account's related contact
    * @param schedulableContext is a context variable which store the runtime information
    */
    global void execute( SchedulableContext schedulableContext ) {
        VehicleUpcomingExpirationBatch objVehicleUpcomingExpirationBatch = new VehicleUpcomingExpirationBatch();
        Database.executeBatch( objVehicleUpcomingExpirationBatch , Integer.valueOf( System.label.Vehicle_Upcoming_Expiration_Batch_Size ) );
    }
}