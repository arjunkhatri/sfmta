@isTest
private class VehicleUpcomingExpirationSchedulerTest {
    
    /**********************************************************************************************************************
    * @description This method notify to the Account's related contacts for vehicle registrations expiring in next 14 days
    */
    public static testMethod void testExecute() {
        Test.startTest();
            VehicleUpcomingExpirationScheduler objVehicleUpcomingExpirationScheduler = new VehicleUpcomingExpirationScheduler();
            String schCron = '0 0 0 * * ? *';
            String jobId = System.Schedule('Test Email send',schCron,objVehicleUpcomingExpirationScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
            SELECT Id
                 , CronExpression
                 , TimesTriggered
                 , NextFireTime
            FROM CronTrigger
           WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression  , 'Cron expression should match' );
        System.assertEquals( 0, cronTrigger.TimesTriggered  , 'Time to Triggered batch should be 0' );
        List<AsyncapexJob> apexJobs = [
            SELECT Id, ApexClassID ,JobType , Status 
            FROM AsyncapexJob
            WHERE JobType = 'BatchApex'];
        System.assertEquals( false , apexJobs.isEmpty()  , 'Expected at least one Async apex Job' );
        System.assertEquals( 'BatchApex', apexJobs[0].JobType  , 'Job Type should match ' );
    }
    
}